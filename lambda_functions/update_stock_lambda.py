import json
import boto3
import os

sns_client = boto3.client('sns')
secrets_manager = boto3.client('secretsmanager')


def get_secret():

    secret_name = "myawsApp"
    response = secrets_manager.get_secret_value(SecretId=secret_name)
    secret_data = json.loads(response['SecretString'])
    return secret_data
    
    
secrets = get_secret()
# print("secrets:", secrets)
    
aws_access_key = secrets.get('AWS_ACCESS_KEY')
aws_secret_key = secrets.get('AWS_SECRET_KEY')
aws_region = secrets.get('AWS_REGION')

dynamodb = boto3.client(
    'dynamodb',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key= aws_secret_key,
    region_name=aws_region)
    
sns_client = boto3.client(
    'sns',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
    region_name=aws_region)
    
    
def lambda_handler(event, context):
    # TODO implement
    print("Event Received from Process-order Topic:", event['Records'][0]['Sns']['Message'])

    for record in event['Records']:
        sns_message = json.loads(record['Sns']['Message'])
        product_id = sns_message['productID']
        quantity = sns_message['quantity']
    
        # Update the stock for the specified product
        update_stock(product_id, quantity)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Srock updated successfully!!')
    }


def update_stock(product_id, quantity_to_subtract):
    # Define the table name
    table_name = os.environ["INVENTORY_TABLE"]
    
    try:
        # Update the item in the DynamoDB table by subtracting the quantity
        response = dynamodb.update_item(
            TableName=table_name,
            Key={'ProductID': {'N': product_id}},
            UpdateExpression='SET QuantityInStock = QuantityInStock - :qty',
            ExpressionAttributeValues={':qty': {'N': str(quantity_to_subtract)}},
            ReturnValues='ALL_NEW'
        )
        
        # Log the response for troubleshooting (optional)
        print("Update Stock Item Succeeded:", response)
    except Exception as e:
        print("Error:", e)