import json
import boto3
import os

sns_client = boto3.client('sns')
secrets_manager = boto3.client('secretsmanager')

def get_secret():

    secret_name = "myawsApp"
    response = secrets_manager.get_secret_value(SecretId=secret_name)
    secret_data = json.loads(response['SecretString'])
    return secret_data
    
    
secrets = get_secret()
print("secrets:", secrets)
    
aws_access_key = secrets.get('AWS_ACCESS_KEY')
aws_secret_key = secrets.get('AWS_SECRET_KEY')
aws_region = secrets.get('AWS_REGION')

dynamodb = boto3.client(
    'dynamodb',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key= aws_secret_key,
    region_name=aws_region)
    
sns_client = boto3.client(
    'sns',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
    region_name=aws_region)
    
    
def lambda_handler(event, context):
    # TODO implement
    # print("MY_EVENT",event)

    for record in event['Records']:
        # Parse the SNS message from each record
        sns_message = json.loads(record['Sns']['Message'])
        # print("Mesage from SNS",sns_message)    
        # Process the order for each message
        process_order(sns_message)
        
        # sns_topic_arn = secrets.get('APP_PROCESS_ORDER_TOPIC_ARN')
        sns_topic_arn = os.environ["APP_PROCESS_ORDER_TOPIC_ARN"]
        sns_message_for_stock_update = {
            'productID': sns_message['ProductID'],
            'quantity': sns_message['Quantity']
        }  
        
        if publish_to_sns_topic(sns_topic_arn, sns_message_for_stock_update):
            return {
                'statusCode': 200,
                'body': json.dumps('Order Processed successfully and published to SNS.')
            }
        else:
            return {
                'statusCode': 500,
                'body': json.dumps('Failed to publish order to SNS.')
            }
    return {
        'statusCode': 200,
        'body': json.dumps('Order Processed successfully and published to SNS')
    }   

def process_order(order_details):
    # Extract order details
    customer_id = order_details['CustomerID']
    product_id = order_details['ProductID']
    quantity = order_details['Quantity']
    
    # Define the table name 
    table_name = os.environ["ORDER_TABLE"]
    
    # Create a new item to be inserted into the DynamoDB table
    order_item = {
        'OrderID' :{'N':str(1)},
        'CustomerID': {'N': customer_id},
        'ProductID': {'N': product_id},
        'Quantity': {'N': str(quantity)},
        'OrderStatus': {'S': 'processed'}
    }
    
    try:
        # Put the item into the DynamoDB table
        response = dynamodb.put_item(TableName=table_name, Item=order_item)
        
        # Log the response for troubleshooting (optional)
        print("PutItem Succeeded:", response)
        
    except Exception as e:
        print("Error:", e)
        

 
def publish_to_sns_topic(topic_arn, message):
    try:
        sns_client.publish(
            TopicArn=topic_arn,
            Message=json.dumps(message)
        )
        return True
    except Exception as e:
        print(f"Error publishing message to SNS: {e}")
        return False