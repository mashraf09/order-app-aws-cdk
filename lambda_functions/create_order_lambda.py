import json
import boto3
import os 

sns_client = boto3.client('sns')
secrets_manager = boto3.client('secretsmanager')


def get_secret():

    secret_name = "myawsApp"
    response = secrets_manager.get_secret_value(SecretId=secret_name)
    secret_data = json.loads(response['SecretString'])
    return secret_data
    
    
secrets = get_secret()
print("secrets:", secrets)
    
aws_access_key = secrets.get('AWS_ACCESS_KEY')
aws_secret_key = secrets.get('AWS_SECRET_KEY')
aws_region = secrets.get('AWS_REGION')

dynamodb = boto3.client(
    'dynamodb',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key= aws_secret_key,
    region_name=aws_region)
    
sns_client = boto3.client(
    'sns',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
    region_name=aws_region)

def lambda_handler(event, context):

    # Check if the event is a POST request
    if event['httpMethod'] != 'POST':
        return {
            'statusCode': 400,
            'body': json.dumps('Invalid HTTP method. Only POST is allowed.')
        }

    # Parse the request body as JSON
    try:
        request_data = json.loads(event['body'])
    except json.JSONDecodeError:
        return {
            'statusCode': 400,
            'body': json.dumps('Invalid JSON data in the request body.')
        }

    # Check for required keys (CustomerID, ProductID, Quantity)
    required_keys = ['CustomerID', 'ProductID', 'Quantity']
    for key in required_keys:
        if key not in request_data:
            return {
                'statusCode': 400,
                'body': json.dumps(f'Missing required key: {key}')
            }
    
     # Check if Quantity is a positive number greater than 0
    try:
        quantity = float(request_data['Quantity'])
        if quantity <= 0:
            return {
                'statusCode': 400,
                'body': json.dumps('Quantity must be a positive number greater than 0.')
            }
    except ValueError:
        return {
            'statusCode': 400,
            'body': json.dumps('Quantity must be a numeric value.')
        }
    
    # Check if CustomerID exists in DynamoDB
    customer_id = request_data['CustomerID']
    print("customer_id", customer_id)
    customer_exists = check_if_item_exists(os.environ["CUSTOMER_TABLE"], 'CustomerID', customer_id)
    if not customer_exists:
        return {
            'statusCode': 400,
            'body': json.dumps(f'CustomerID {customer_id} not found in the database.')
        }

    # Check if ProductID exists in DynamoDB
    product_id = request_data['ProductID']
    product_exists = check_if_item_exists(os.environ["INVENTORY_TABLE"], 'ProductID', product_id)
    if not product_exists:
        return {
            'statusCode': 400,
            'body': json.dumps(f'ProductID {product_id} not found in the database.')
        }

    # Publish the data to the SNS topic
    # sns_topic_arn = secrets.get('APP_CREATE_ORDER_TOPIC_ARN')
    sns_topic_arn = os.environ["APP_CREATE_ORDER_TOPIC_ARN"]
    sns_message = request_data  # Assuming request_data is a dictionary
    if publish_to_sns_topic(sns_topic_arn, sns_message):
        return {
            'statusCode': 200,
            'body': json.dumps('Order created successfully and published to SNS.')
        }
    else:
        return {
            'statusCode': 500,
            'body': json.dumps('Failed to publish order to SNS.')
        }


def check_if_item_exists(table_name, key_name, key_value):
    # print("check_if_item_exists called")
    # print(table_name, key_name, key_value)
    response = dynamodb.get_item(
        TableName=table_name,
        Key={key_name: {'N': key_value}}
    )
    return 'Item' in response

 
def publish_to_sns_topic(topic_arn, message):
    try:
        sns_client.publish(
            TopicArn=topic_arn,
            Message=json.dumps(message)
        )
        return True
    except Exception as e:
        print(f"Error publishing message to SNS: {e}")
        return False