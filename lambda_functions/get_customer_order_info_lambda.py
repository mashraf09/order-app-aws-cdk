import json
import boto3
import os

secrets_manager = boto3.client('secretsmanager')


def get_secret():

    secret_name = "myawsApp"
    response = secrets_manager.get_secret_value(SecretId=secret_name)
    secret_data = json.loads(response['SecretString'])
    return secret_data
    
    
secrets = get_secret()
# print("secrets:", secrets)
    
aws_access_key = secrets.get('AWS_ACCESS_KEY')
aws_secret_key = secrets.get('AWS_SECRET_KEY')
aws_region = secrets.get('AWS_REGION')

dynamodb = boto3.client(
    'dynamodb',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key= aws_secret_key,
    region_name=aws_region)
    
def get_customer_info(customer_id):
    # Define the table name
    table_name = os.environ["CUSTOMER_TABLE"]

    try:
        # Query the DynamoDB table to get customer information by CustomerID
        response = dynamodb.query(
            TableName=table_name,
            KeyConditionExpression='CustomerID = :customer_id',
            ExpressionAttributeValues={':customer_id': {'N': customer_id}}
        )

        # Extract the customer information from the response
        customer_info = response.get('Items', [])[0] if response.get('Count', 0) > 0 else None

        return customer_info
    except Exception as e:
        print("Error:", e)
        return None

def get_product_info(product_id):
    # Define the table name
    table_name = os.environ["INVENTORY_TABLE"]

    try:
        # Query the DynamoDB table to get product information by ProductID
        response = dynamodb.query(
            TableName=table_name,
            KeyConditionExpression='ProductID = :product_id',
            ExpressionAttributeValues={':product_id': {'': product_id}}
        )

        # Extract the product information from the response
        product_info = response.get('Items', [])[0] if response.get('Count', 0) > 0 else None

        return product_info
    except Exception as e:
        print("Error:", e)
        return None

def get_orders_by_customer(customer_id):
    # Define the table name
    table_name =  os.environ["ORDER_TABLE"]

    try:
        # Query the DynamoDB table to get orders by CustomerID
        response = dynamodb.scan(
            TableName=table_name,
            FilterExpression='CustomerID = :customer_id',
            ExpressionAttributeValues={':customer_id': {'N': customer_id}}
        )

        # Extract the list of orders from the response
        orders = response.get('Items', [])
        print(orders)
        return orders
    except Exception as e:
        print("Error:", e)
        return []

def enrich_order_info(orders):
    enriched_orders = []
    for order in orders:
        product_id = order.get('ProductID', {}).get('S', '')
        quantity = int(order.get('Quantity', {}).get('N', '0'))
        product_info = get_product_info(product_id)
        order['ProductInfo'] = product_info
        enriched_orders.append(order)
    return enriched_orders

def lambda_handler(event, context):
    # Extract the CustomerID from the query parameters
    query_params = event.get('queryStringParameters', {})

    # Get the value of the 'CustomerID' parameter
    customer_id = query_params.get('CustomerID')

    if customer_id is  None:
         return {
            'statusCode': 400,
            'body': json.dumps({'message': 'CustomerID not provided'})
        }

    print("customer_id", customer_id)
    # Get customer information
    customer_info = get_customer_info(customer_id)
   

    # Get orders by CustomerID
    orders = get_orders_by_customer(customer_id)

    # Enrich the order information with product details
    # enriched_orders = enrich_order_info(orders)

    # Return a response with customer information and enriched orders
    response = {
        'statusCode': 200,
        'body': json.dumps({
            'CustomerInfo': customer_info,
            
            'Orders': orders
        })
    }

    return response