from aws_cdk import (
    # Duration,
    aws_lambda as _lambda,
    Stack,
    aws_sns as sns,
    aws_sns_subscriptions as sns_subscriptions,
    aws_apigateway as apigw,
    aws_ssm as ssm,
    aws_secretsmanager as secretsmanager,
    aws_iam as iam,
    aws_dynamodb as dynamodb
    # aws_sqs as sqs,
)
from constructs import Construct

class OrderAppCdkStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # SNS Topic createion
        create_order_sns_topic = sns.Topic(
            self,
            "create_order_topic",
            display_name="create_order_topic",  # Optional: Set a display name
        )

        process_order_sns_topic = sns.Topic(
            self,
            "process_order_topic",
            display_name="process_order_topic",  
        )
            
        # Define a DynamoDB table
        order_table = dynamodb.Table(
            self,
            "Order",
            partition_key={"name": "OrderID", "type": dynamodb.AttributeType.NUMBER},
            billing_mode=dynamodb.BillingMode.PAY_PER_REQUEST,
            table_name="Order-cdk"  
        )

        customer_table = dynamodb.Table(
            self,
            "Customer",
            partition_key={"name": "CustomerID", "type": dynamodb.AttributeType.NUMBER},
            billing_mode=dynamodb.BillingMode.PAY_PER_REQUEST,
            table_name="Customer-cdk" 
        )

        inventory_table = dynamodb.Table(
            self,
            "Inventory",
            partition_key={"name": "ProductID", "type": dynamodb.AttributeType.NUMBER},
            billing_mode=dynamodb.BillingMode.PAY_PER_REQUEST,
            table_name="Inventory-cdk"
        )
        
        env_variables = {}
        env_variables["APP_CREATE_ORDER_TOPIC_ARN"] = create_order_sns_topic.topic_arn
        env_variables["APP_PROCESS_ORDER_TOPIC_ARN"] = process_order_sns_topic.topic_arn
        env_variables["ORDER_TABLE"] = order_table.table_name
        env_variables["INVENTORY_TABLE"] = inventory_table.table_name
        env_variables["CUSTOMER_TABLE"] = customer_table.table_name


        create_order_lambda  = _lambda.Function(
            self, 'create_order_lambda',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='create_order_lambda.lambda_handler',
            code=_lambda.Code.from_asset('lambda_functions'), 
            environment=env_variables,
            function_name='create_order_lambda'
        )

        process_order_lambda  = _lambda.Function(
            self, 'process_order_lambda',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='process_order_lambda.lambda_handler',
            code=_lambda.Code.from_asset('lambda_functions'),
            environment=env_variables,
            function_name='process_order_lambda'
        )

        update_stock_lambda  = _lambda.Function(
            self, 'update_stock_lambda',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='update_stock_lambda.lambda_handler',
            code=_lambda.Code.from_asset('lambda_functions'),
            environment=env_variables,
            function_name='update_stock_lambda'
        )

        get_customer_order_info_lambda  = _lambda.Function(
            self, 'get_customer_order_info_lambda',
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler='get_customer_order_info_lambda.lambda_handler',
            code=_lambda.Code.from_asset('lambda_functions'),
            environment=env_variables,
            function_name='get_customer_order_info_lambda'
        )


        # Add the Lambda function as a subscription to the SNS topic
        create_order_sns_topic.add_subscription(sns_subscriptions.LambdaSubscription(process_order_lambda))
        process_order_sns_topic.add_subscription(sns_subscriptions.LambdaSubscription(update_stock_lambda))

         # Grant the Lambda function permission to read/write from the DynamoDB table
        order_table.grant_full_access(process_order_lambda)
        order_table.grant_full_access(get_customer_order_info_lambda)
        order_table.grant_full_access(create_order_lambda)
        order_table.grant_full_access(update_stock_lambda)
  
            

        customer_table.grant_full_access(process_order_lambda)
        customer_table.grant_full_access(get_customer_order_info_lambda)
        customer_table.grant_full_access(create_order_lambda)
        customer_table.grant_full_access(update_stock_lambda)
        customer_table.grant_full_access(create_order_lambda)

        inventory_table.grant_full_access(process_order_lambda)
        inventory_table.grant_full_access(get_customer_order_info_lambda)
        inventory_table.grant_full_access(create_order_lambda)
        inventory_table.grant_full_access(update_stock_lambda)
      

        # API Gateway creation
        order_app_rest_api = apigw.RestApi(
            self, 'order_app_apis',
            deploy_options={
            "stage_name": "order-app-v1"
            }
            )
        
        create_order_lambda_integration = apigw.LambdaIntegration(
            create_order_lambda,
            proxy=True  # Use "proxy" if you want the entire request to be passed to your Lambda function   
            )
        
        order_resource = order_app_rest_api.root.add_resource('order')
        method = order_resource.add_method('POST', create_order_lambda_integration)


        get_customer_order_info_lambda_integration = apigw.LambdaIntegration(
            get_customer_order_info_lambda,
            proxy=True  # Use "proxy" if you want the entire request to be passed to your Lambda function   
            )
        
        order_resource = order_app_rest_api.root.add_resource('customer_order')
        method = order_resource.add_method('GET', get_customer_order_info_lambda_integration)

        # Define a policy statement allowing GetSecretValue on the secret
        secrets_manager_policy_statement = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=["secretsmanager:*"],
            resources=["*"]
                # f"arn:aws:secretsmanager:{self.region}:{self.account}:secret/myawsApp"],
        )
         # Attach the policy statement to the Lambda function's role
        create_order_lambda.add_to_role_policy(secrets_manager_policy_statement)
        process_order_lambda.add_to_role_policy(secrets_manager_policy_statement)
        update_stock_lambda.add_to_role_policy(secrets_manager_policy_statement)
        get_customer_order_info_lambda.add_to_role_policy(secrets_manager_policy_statement)
